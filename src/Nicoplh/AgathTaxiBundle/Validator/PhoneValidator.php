<?php
/**
 * Created by JetBrains PhpStorm.
 * User: xadozuk
 * Date: 26/04/13
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */

namespace Nicoplh\AgathTaxiBundle\Validator;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PhoneValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if(!empty($value) && !preg_match('/^0[1-689][0-9]{8}$/', $value))
            $this->context->addViolation($constraint->message, array('%value%' => $value));
    }
}