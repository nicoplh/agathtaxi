<?php
/**
 * Created by JetBrains PhpStorm.
 * User: xadozuk
 * Date: 26/04/13
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */

namespace Nicoplh\AgathTaxiBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Class Phone
 * @package Nicoplh\AgathTaxiBundle\Validator
 * @Annotation
 */
class Phone extends Constraint
{
    public $message = "The telephone number %value% is not valid";
}