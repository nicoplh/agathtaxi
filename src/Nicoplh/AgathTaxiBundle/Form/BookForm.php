<?php
/**
 * Created by JetBrains PhpStorm.
 * User: xadozuk
 * Date: 26/04/13
 * Time: 11:24
 * To change this template use File | Settings | File Templates.
 */

namespace Nicoplh\AgathTaxiBundle\Form;

use Nicoplh\AgathTaxiBundle\Validator\Phone;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Time;

class BookForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lastName',   'text', array(
                'attr' => array(
                    'class'         => 'span6',
                    'placeholder'   => 'placeholder.lastname'
                ),
                'constraints' => array(
                    new NotBlank()
                )
            ))
        ->add('firstName',  'text', array(
                'attr' => array(
                    'class'         => 'span6',
                    'placeholder'   => 'placeholder.firstname'
                ),
                'constraints' => array(
                    new NotBlank()
                )
            ))
        ->add('phone',      'text', array(
                'attr' => array(
                    'class'         => 'span6',
                    'placeholder'   => 'placeholder.phone'
                ),
                'constraints' => array(
                    new NotBlank(),
                    new Phone()
                )
            ))
        ->add('email',      'email', array(
                'attr' => array(
                    'class'         => 'span6',
                    'placeholder'   => 'placeholder.mail'
                ),
                'constraints' => array(
                    new NotBlank(),
                    new Email()
                )
            ))
        ->add('start',      'text', array(
                'attr' => array(
                    'class'         => 'span6',
                    'placeholder'   => 'placeholder.address'
                ),
                'constraints' => array(
                    new NotBlank()
                )
            ))
        ->add('destination','text', array(
                'attr' => array(
                    'class'         => 'span6',
                    'placeholder'   => 'placeholder.destination'
                ),
                'constraints' => array(
                    new NotBlank()
                )
            ))
        ->add('date',       'date', array(
                'widget' => 'single_text',
                'attr' => array(
                    'class'         => 'span6',
                    'placeholder'   => 'placeholder.date'
                ),
                'constraints' => array(
                    new NotBlank(),
                    new Date()
                )
            ))
        ->add('time',       'time', array(
                'widget' => 'single_text',
                'attr' => array(
                    'class'         => 'span6',
                    'placeholder'   => 'placeholder.time'
                ),
                'constraints' => array(
                    new NotBlank(),
                    new Time()
                )
            ))
        ->add('message',   'textarea', array(
                'label' => 'placeholder.message',
                'required' => false,
                'attr' => array(
                    'class'         => 'input-block-level',
                )
            ));
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'booking';
    }
}