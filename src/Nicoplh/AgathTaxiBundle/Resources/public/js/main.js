$(document).ready(function()
{
    var map = function(mapCanvas)
    {
        var pos = new google.maps.LatLng(43.284851, 3.498561);

        var options =
        {
            zoom:               17,
            center:             pos,
            mapTypeId:          google.maps.MapTypeId.HYBRID,
            mapMarker:          true,
            panControl:         false,
            streetViewControl:  false,
            zoomControl:        true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.TOP_LEFT
            }
        };

        map = new google.maps.Map(mapCanvas, options);

        new google.maps.Marker({
            position:   pos,
            map:        map
        });
    };

    if($('#contact-map').length > 0)
    {
        map($('#contact-map').get(0));
    }

    $('.modal-trigger').click(function(e)
    {
        e.stopPropagation();
        e.preventDefault();

        var modal = $(this).attr('data-target');

        $('.gallery-overview', modal)
            .attr('src', $(this).attr('href'));

        $(modal).modal();
    });

    var handleForm = function(context, modal, url)
    {
        $('.loading', modal).fadeOut('slow', function()
        {
            $(context).fadeIn('slow');
        });

        //Gestion formulaire
        $('form', context).submit(function(e)
        {
            e.preventDefault();
            e.stopPropagation();

            var form = this;

            $(context).fadeOut('slow', function()
            {
                $('.loading', modal).fadeIn('slow');

                $.post(url, $(form).serialize(), function(data)
                {
                    $(context).html($(data).find('.async-content:first'));

                    handleForm(context, modal, url);
                });
            });

            return false;
        });
    };

    $('.async-modal-trigger').click(function(e)
    {
        e.preventDefault();
        e.stopPropagation();

        var modal = $(this).attr('data-target');
        var url = $(this).attr('href');

        $(modal).modal({
            'backdrop': 'static',
            'keyboard': false
        });

        $('.async-content', modal).hide().load(url + " .async-content:first", function()
        {
            handleForm(this, modal, url)
        });

        $(modal).on('hidden', function()
        {
            $('.loading', $(modal)).show();
            $('.async-content', modal).text('');
        });

        return false;
    });

    $('a[data-toggle="tooltip"]').tooltip();

    $('#acceuil-carousel').carousel({
        interval: 3000,
        pause: ""
    });

    $('#gallery-carousel').carousel({
        interval: 2500,
        pause: ""
    });

    $('a[data-toggle="tooltip"]').click(function(e)
    {
        e.preventDefault();
        e.stopPropagation();
        return false;
    });
});