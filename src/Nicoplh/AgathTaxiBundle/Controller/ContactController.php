<?php

namespace Nicoplh\AgathTaxiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contact_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}