<?php

namespace Nicoplh\AgathTaxiBundle\Controller;

use Nicoplh\AgathTaxiBundle\Form\BookForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class BookingController extends Controller
{
    /**
     * @Route("/reservation", name="booking_index")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(new BookForm());

        if($request->getMethod() == 'POST')
        {
            $form->bind($request);

            if($form->isValid())
            {
                $bookForm = $form->getData();
                $message = \Swift_Message::newInstance()
                    ->setSubject('Réservation Taxi')
                    ->setFrom($bookForm['email'])
                    ->setTo('nicosplh@hotmail.fr')
                    ->setBody(
                        $this->renderView(
                            'NicoplhAgathTaxiBundle:Booking:email.html.twig',
                            array('params' => $bookForm)
                        )
                    );
                $this->get('mailer')->send($message);

                $this->get('session')->getFlashBag()->add('success', 'Votre message à été envoyé, il sera traité dans la journée ! Merci');

            }
        }

        return array(
            'form' => $form->createView()
        );
    }
}