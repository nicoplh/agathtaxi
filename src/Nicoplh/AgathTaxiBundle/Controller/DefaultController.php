<?php

namespace Nicoplh\AgathTaxiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="default_index", defaults={"_locale"="fr"})
     *
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}
