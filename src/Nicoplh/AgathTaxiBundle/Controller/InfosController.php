<?php

namespace Nicoplh\AgathTaxiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class InfosController extends Controller
{
    /**
     * @Route("/infos", name="infos_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}