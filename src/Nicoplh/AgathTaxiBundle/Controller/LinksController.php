<?php

namespace Nicoplh\AgathTaxiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class LinksController extends Controller
{
    /**
     * @Route("/liens", name="links_index", defaults={"_locale"="fr"})
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}